# Ethereum Transactions Analyzer

This is a simple JavaScript script that allows you to analyze the buying and selling activity of a specific Polygon address on the Polygon network. It uses the Polygon API to retrieve transaction data for the given address and then parses the data to determine if the address has bought or sold any tokens. The script also filters out the transactions which are not smart contract transactions.

## Usage

1. Clone or download this repository
2. Run `npm install` to install the required dependencies (axios and fast-csv)
3. Open `analysis.js` and replace `address` with the Polygon address you want to analyze
4. Run the script with `node analysis.js`
5. The script will print the transaction details to the console and also save the transaction details in a csv file named transactions.csv

## Dependencies

This script requires the following npm packages:

- axios: for making HTTP requests
- fast-csv: for writing the data to a csv file

## Note

This script uses the Polygon Network and Polygon API key. Make sure you use your own API key if you want to use it on other networks or on mainnet.
